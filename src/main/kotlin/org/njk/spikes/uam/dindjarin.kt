package org.njk.spikes.uam

import arrow.core.*
import arrow.core.extensions.fx
import org.njk.faults.Fault
import java.util.regex.Pattern

enum class FaultType {
  Configuration,
  Inconsistency,
  Invalidity
}

// A Principal is an entity who can be granted privileges
interface Principal {
  val id: String
  val name: String
}

// A subject is a type of a principal for whom the entire tree resolution happens
interface Subject : Principal

// User is a subject
data class User(override val id: String, override val name: String) : Subject

// Group is a Principal
data class Group(override val id: String, override val name: String, val principals: List<String>) : Principal

/*
 * A permission string is a URL/query string inspired specification of what permission is being granted
 * The resource part of permission can be nested like URLs. eg /invoice/create or /mysvc/myresource/myaction
 * This allows wildcarding to be implemented (eg grant permission to /mysvc<slash>* for admin privileges)
 * This should be useful for API based services since it can map on straight to the URL if required
 *
 * In addition optional constraints can be specified
 * eg: contract=c1. In such a case the authorisation call will need to pass the instance value for contract, and it will need to be exactly c1
 * Multiple constraints can be specified like a query string eg. contract=c1&branch=b1
 *
 * At the moment only a single instance value can be specified, but this will need to be extended
 */

data class Permission(
  val resource: String,
  val constraints: List<Pair<String, String>>
) {
  companion object {
    val PERMISSION_PATTERN = Pattern.compile("^((/\\w+)+)(\\?(\\w+)=(\\w+)(\\&(\\w+)=(\\w+))*)?")
    fun fromString(permissionString: String): Either<Fault<FaultType>, Permission> =
      PERMISSION_PATTERN.matcher(permissionString).let { matcher ->
        if (matcher.matches()) {
          val constraintString = matcher.group(3)
          val constraints = constraintString
            ?.trim()
            ?.substring(1)
            ?.split("&")
            ?.map { it.split("=").let { it[0] to it[1] } }
            ?: listOf<Pair<String, String>>()
          Permission(matcher.group(1), constraints).right()
        } else {
          Fault(
            FaultType.Inconsistency,
            "err-invalid-permission-str",
            mapOf("str" to permissionString)
          ).left()
        }
      }

  }

  val elements = resource.trim().substring(1).split("/")
}


// A role is a collection of permissions
class Role(val id: String, val name: String, val permissionStrings: List<String>)

// A grant associates a role to a principal
class Grant(val principalId: String, val role: String)

// This is used for internal processing to build up necessary information
data class GroupDetails(val id: String, val name: String, val permissions: List<String>)
// This is used for internal processing to build up necessary information
// It needs to be figured out exactly what will be cached vs what will go to the database always
data class SubjectDetails(val id: String, val name: String, val permissions: Map<String, Permission>) {
  fun allow(resource: String, instanceValues: Map<String,String>): Either<Fault<FaultType>, Boolean> =
    permissions[resource]?.constraints?.let {
      it.fold(true.right() as Either<Fault<FaultType>, Boolean>) { acc, elem ->
        acc.flatMap { allow ->
          if (allow) {
            // fixme: needs to be more advanced than just a string comparison
            if (instanceValues[elem.first] != elem.second) {
              false.right()
            } else {
              allow.right()
            }
          } else allow.right()
        }
      }
    } ?: false.right()

}

class AuthorisationDatabase(principals: List<Principal>, roles: List<Role>, grants: List<Grant>) {
  val principals = principals.map { it.id to it }.toMap()
  val roles = roles.map { it.id to it }.toMap()
  val userGroups =
    principals.filterIsInstance(Group::class.java).flatMap { group -> group.principals.map { it to group.id } }
      .groupBy({ it.first }, { it.second })
  val grants = grants.groupBy({ it.principalId }, { it.role })

  fun getPrincipalForId(subjectId: String) =
    principals[subjectId].rightIfNotNull {
      Fault(FaultType.Invalidity, "err-principal-not-found", mapOf("id" to subjectId))
    }

  fun getUserGroupsForUser(subjectId: String) =
    userGroups[subjectId].rightIfNotNull {
      Fault(FaultType.Inconsistency, "err-principal-not-found", mapOf("id" to subjectId))
    }

  fun getRolesForUserAndGroups(subjectId: String, groupIds: List<String>) =
    (listOf(subjectId) + groupIds).flatMap { grants[it] ?: emptyList() }.right()

  fun getRoleForRoleId(roleId: String) = roles[roleId].rightIfNotNull {
    Fault(FaultType.Inconsistency, "err-role-not-found", mapOf("id" to roleId))
  }

  fun getPermissionsForPermissionStrings(permissionIds: List<String>): Either<Fault<FaultType>, List<Permission>> =
    permissionIds.fold(listOf<Permission>().right() as Either<Fault<FaultType>, List<Permission>>) { acc, elem ->
      Permission.fromString(elem).flatMap { perm ->
        acc.flatMap { list -> (list + perm).right() }
      }
    }

  fun getPermissionsForRoles(roleIds: List<String>): Either<Fault<FaultType>, List<Permission>> =
    roleIds.fold(listOf<Permission>().right() as Either<Fault<FaultType>, List<Permission>>) { acc, elem ->
      acc.flatMap { permissions ->
        getRoleForRoleId(elem).flatMap {
          getPermissionsForPermissionStrings(it.permissionStrings)
        }
      }
    }
}

// This is the service
class Authoriser(val db: AuthorisationDatabase) {
  fun List<Permission>.merge() = elementAt(0) // fixme: the constraints need to be merged into a single permission if for the same resource
  fun List<Permission>.toPermissionMap() = this.groupBy { it.resource }.mapValues { it.value.merge() }
  fun getSubjectDetails(subjectId: String): Either<Fault<FaultType>, SubjectDetails> =
    Either.fx<Fault<FaultType>, SubjectDetails> {
      val user = !db.getPrincipalForId(subjectId)
      val userGroups = !db.getUserGroupsForUser(subjectId)
      val roleIds = !db.getRolesForUserAndGroups(subjectId, userGroups)
      val permissions: List<Permission> = !db.getPermissionsForRoles(roleIds)
      SubjectDetails(subjectId, user.name, permissions.toPermissionMap())
    }

  fun authorise(subjectId: String, resource: String, instanceValues: Map<String,String>): Either<Fault<FaultType>, Boolean> =
    Either.fx<Fault<FaultType>, Boolean> {
      val subjectDetails = !getSubjectDetails(subjectId)
      !subjectDetails.allow(resource, instanceValues)
    }
}


fun main(args: Array<String>) {
  val users = listOf(
    User("u1", "User 1"),
    User("u2", "User 2"),
    User("u3", "User 3"),
    User("u4", "User 4"),
    User("u5", "User 5"),
    User("u6", "User 6"),
    User("u7", "User 7"),
    User("u8", "User 8"),
    User("u9", "User 9"),
    User("u10", "User 10"),
    User("u11", "User 11"),
    User("u12", "User 12"),
  )

  val roles = listOf(
    Role(
      "inv-makers", "Invoice Makers", listOf(
        "/invoice/create?contract=c1&branch=b1", "/myapp/invoice/view", "/yourapp"
      )
    )
  )

  val groups = listOf(Group("c1-makers", "C1 Makers", listOf("u1", "u2")))

  val grants = listOf(Grant("c1-makers", "inv-makers"))
  val db = AuthorisationDatabase(users + groups, roles, grants)
  val authoriser = Authoriser(db)

  println(authoriser.authorise("u1", "/invoice/create", mapOf("contract" to "c1"))) // false, branch not specified
  println(authoriser.authorise("u1", "/invoice/create", mapOf("contract" to "c1", "branch" to "b1")))
  println(authoriser.authorise("u1", "/invoice/view", mapOf("contract" to "c1", "branch" to "b1"))) // no access to invoice view
}
