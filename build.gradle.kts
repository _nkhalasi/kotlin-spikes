plugins {
    kotlin("jvm") version "1.4.21"
}

group = "org.njk"
version = "1.0-SNAPSHOT"

val arrowKtVersion = "0.11.0"
val jacksonVersion = "2.12.0"
val logbackVersion = "1.2.3"
val jodatimeVersion = "2.10.9"

repositories {
    mavenCentral()
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
    kotlinOptions.jvmTarget = "1.8"
}

apply(plugin = "org.jetbrains.kotlin.jvm")
apply(plugin = "kotlin-kapt")

dependencies {
    implementation(kotlin("stdlib"))
    implementation("io.arrow-kt", "arrow-core", arrowKtVersion)
    implementation("io.arrow-kt", "arrow-fx", arrowKtVersion)
    implementation("io.arrow-kt", "arrow-mtl", arrowKtVersion)
    implementation("io.arrow-kt", "arrow-syntax", arrowKtVersion)
    api("com.fasterxml.jackson.datatype", "jackson-datatype-jsr310", jacksonVersion)
    api("com.fasterxml.jackson.datatype", "jackson-datatype-jdk8", jacksonVersion)
    api("com.fasterxml.jackson.module", "jackson-module-kotlin", jacksonVersion)
    implementation("ch.qos.logback", "logback-core", logbackVersion)
    implementation("ch.qos.logback", "logback-classic", logbackVersion)
    implementation("joda-time", "joda-time", jodatimeVersion)
}
