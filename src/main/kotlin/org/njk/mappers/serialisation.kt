package org.njk.mappers

import arrow.core.*
import catchT
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.node.TextNode
import com.fasterxml.jackson.module.kotlin.jacksonTypeRef
import org.njk.faults.Fault
import java.security.InvalidParameterException
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormatter
import kotlin.collections.mapOf

// Option
class OptionSerializer<T : Option<*>> : JsonSerializer<T>() {
  override fun serialize(value: T?, gen: JsonGenerator?, serializers: SerializerProvider?) {
    gen?.let { tmpGen ->
      value?.let { tmpValue ->
        tmpValue.fold(
          {
            tmpGen.writeStartObject()
            tmpGen.writeFieldName("opt")
            tmpGen.writeString("None")
            tmpGen.writeEndObject()
          },
          {
            if (it != null) {
              tmpGen.writeStartObject()
              tmpGen.writeFieldName("opt")
              tmpGen.writeString("Some")
              tmpGen.writeFieldName("class")
              tmpGen.writeString(it.javaClass.name)
              tmpGen.writeFieldName("value")
              tmpGen.writeObject(it)
              tmpGen.writeEndObject()
            }
          }
        )
      }
    }
  }
}

class OptionDeserializer<T : Option<*>> : JsonDeserializer<T>() {
  @Suppress("UNCHECKED_CAST")
  override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): T? {
    if (ctxt != null && p != null) {
      if (p.currentToken == JsonToken.START_OBJECT) {
        val map = p.readValueAsTree<TreeNode>()
        when (map["opt"]?.let { it as? TextNode }?.textValue()) {
          "Some" -> {
            val value = map["value"]
            val klass = map["class"]?.let { it as? TextNode }?.let { Class.forName(it.textValue()) }
            return (p.codec as? ObjectMapper)!!.convertValue(value, klass).some() as T
          }
          "None" -> {
            return None as T
          }
        }

      }
    }
    return null
  }
}

// DateTime
class DateTimeSerializer(private val useTimestamp: Boolean, private val customFormat: Option<DateTimeFormatter> = None) : JsonSerializer<DateTime>() {
  private fun timestamp(value: DateTime?): Long = value?.millis ?: 0L

  override fun serialize(value: DateTime?, gen: JsonGenerator?, serializers: SerializerProvider?) {
    if (useTimestamp) {
      gen?.writeNumber(timestamp(value))
    } else {
      customFormat.fold({
        throw InvalidParameterException("err-custom-format-missing")
      }, {
        gen?.writeString(it.print(value))
      })
    }
  }

}

class DateTimeDeserializer(private val useTimestamp: Boolean, private val customFormat: Option<DateTimeFormatter> = None) : JsonDeserializer<DateTime>() {
  private fun fromTimestamp(value: Long) = DateTime(value)

  override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): DateTime? =
    if (ctxt != null && p != null) {
      if (useTimestamp) {
        DateTime(p.longValue)
      } else {
        customFormat.fold({
          throw InvalidParameterException("err-custom-format-missing")
        }, {
          DateTime.parse(p.text, it)
        })
      }
    } else null

}

// Either
class EitherSerializer<T : Either<*, *>> : JsonSerializer<T>() {
  override fun serialize(value: T?, gen: JsonGenerator?, serializers: SerializerProvider?) {
    gen?.let { tmpGen ->
      value?.let { value ->
        value.fold(
          { writeEither(tmpGen, "L", it) },
          { writeEither(tmpGen, "R", it) }
        )
      }
    }
  }

  private fun writeEither(gen: JsonGenerator, type: String, it: Any?) {
    if (it != null) {
      gen.writeStartObject()
      gen.writeFieldName("class")
      gen.writeString(it.javaClass.name)
      gen.writeFieldName("type")
      gen.writeString(type)
      gen.writeFieldName("value")
      gen.writeObject(it)
      gen.writeEndObject()
    } else {
      gen.writeNull()
    }
  }
}

class EitherDeserializer<T : Either<*, *>> : JsonDeserializer<T>() {
  @Suppress("UNCHECKED_CAST")
  override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): T? {
    if (ctxt != null && p != null) {
      if (p.currentToken == JsonToken.START_OBJECT) {
        val map = p.readValueAsTree<TreeNode>()
        val klass = map["class"]?.let { it as? TextNode }?.let { Class.forName(it.textValue()) }
        val type = map["type"]?.let { it as? TextNode }?.textValue()
        val value = map["value"]
        if (klass != null && type != null && value != null) {
          when (type) {
            "L" -> {
              return (p.codec as? ObjectMapper)!!.convertValue(value, klass).left() as T
            }
            "R" -> {
              val mapper = (p.codec as? ObjectMapper)!!
              return mapper.convertValue(value, klass).right() as T
            }
          }
        }
      }
    }
    return null
  }
}


// LocalDateTime serialization
class CustomLocalDateTimeSerializer(private val useTimestamp: Boolean, private val customFormat: Option<java.time.format.DateTimeFormatter> = None) : JsonSerializer<LocalDateTime>() {
  private fun timestamp(value: LocalDateTime?): Long = value?.let {
    it.atZone(ZoneId.systemDefault())?.toInstant()?.toEpochMilli()
  } ?: 0L

  override fun serialize(value: LocalDateTime?, gen: JsonGenerator?, serializers: SerializerProvider?) {
    if (useTimestamp) {
      gen?.writeNumber(timestamp(value))
    } else {
      customFormat.fold({
        throw InvalidParameterException("err-custom-format-missing")
      }, {
        gen?.writeString(value?.format(it)?: "0")
      })
    }
  }

}

class CustomLocalDateTimeDeserializer(private val useTimestamp: Boolean, private val customFormat: Option<java.time.format.DateTimeFormatter> = None) : JsonDeserializer<LocalDateTime>() {
  private fun fromTimestamp(value: Long) =  LocalDateTime.ofInstant(Instant.ofEpochMilli(value), ZoneId.systemDefault());

  override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): LocalDateTime? =
    if (ctxt != null && p != null) {
      if (useTimestamp) {
        fromTimestamp(p.longValue)
      } else {
        customFormat.fold({
          throw InvalidParameterException("err-custom-format-missing")
        }, {
          LocalDateTime.parse(p.text, it)
        })
      }
    } else null

}

// converters

inline fun <E : Enum<E>, reified T> ObjectMapper.readValue(from: String, faultType: E): Either<Fault<E>, T> =
  catchT(faultType, "err-issue-with-deserializing-string", mapOf("target-type" to jacksonTypeRef<T>())) {
    readValue(from, T::class.java)
  }

inline fun <E : Enum<E>, reified T> ObjectMapper.convertValue(from: Any, faultType: E): Either<Fault<E>, T> =
  catchT(faultType, "err-issue-with-deserializing-string", mapOf("target-type" to jacksonTypeRef<T>())) {
    convertValue(from, T::class.java)
  }
