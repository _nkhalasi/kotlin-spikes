import arrow.core.Either
import arrow.core.left
import arrow.core.right
import arrow.core.some
import org.njk.faults.Fault

fun <A, E : Enum<E>> catchE(e: E, message: String, args: Map<String, Any?> = emptyMap(), f: () -> Either<Fault<E>, A>): Either<Fault<E>, A> =
  try {
    f()
  } catch (t: Throwable) {
    t.printStackTrace()
    Fault(e, message, args, cause = t).left()
  }

fun <A, E : Enum<E>> catchT(e: E, message: String, args: Map<String, Any?> = emptyMap(), f: () -> A): Either<Fault<E>, A> =
  try {
    f().right()
  } catch (t: Throwable) {
    t.printStackTrace()
    Fault(e, message, args, cause = t).left()
  }


suspend fun <A, E : Enum<E>> xCatchE(e: E, message: String, args: Map<String, Any?> = emptyMap(), f: suspend () -> Either<Fault<E>, A>): Either<Fault<E>, A> =
  try {
    f()
  } catch (t: Throwable) {
    t.printStackTrace()
    Fault(e, message, args, cause = t).left()
  }

suspend fun <A, E : Enum<E>> xCatchT(e: E, message: String, args: Map<String, Any?> = emptyMap(), f: suspend () -> A): Either<Fault<E>, A> =
  try {
    f().right()
  } catch (t: Throwable) {
    t.printStackTrace()
    Fault(e, message, args, cause = t).left()
  }
