package org.njk.faults

import arrow.core.*
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger

open class Fault<E : Enum<E>>(
  val type: E,
  val message: String,
  val args: Map<String, Any?> = EMPTY_MAP,
  val cause: Throwable? = null,
  @Suppress("UNCHECKED_CAST")
  val contents: List<Fault<E>> = EMPTY_LIST as List<Fault<E>>
) {
  companion object {
    val EMPTY_MAP = kotlin.collections.mapOf<String, Any?>()
    val EMPTY_LIST = emptyList<Fault<*>>()
  }

  override fun toString() = "Fault(${type}:${message}:${args}:${cause})"
  fun toLogString() = "${type}:${message}:${args}"
}


// this helps logging faults as error
fun <E : Enum<E>> Logger.fault(fault: Fault<E>, mapper: ObjectMapper) =
  mapper.writeValueAsString(mapOf("message" to fault.message, "args" to fault.args)).run {
    error(this, fault.cause)
  }

data class FaultReport<E : Enum<E>>(
  val faultType: E,
  val message: String,
  val args: Map<String, Any?>,
  val cause: Throwable?
)

fun <E : Enum<E>> Fault<E>.toReport() = FaultReport(type, message, args, cause)

enum class WaltFaultType(val str: String) {
  Runtime("runtime");

  fun getValue() = str

  companion object {
    private val map = values().map { it.str to it }.toMap()

    operator fun invoke(str: String) = map[str]?.right()
      ?: Fault(Runtime, "err-invalid-faults", mapOf("input-faults-str" to str)).left()
  }
}

typealias WaltFault = Fault<WaltFaultType>

fun <E : Enum<E>, L : Fault<E>, R> List<Either<L, R>>.consolidateLefts(type: E, message: String, args: Map<String, Any?> = Fault.EMPTY_MAP): Either<Fault<E>, List<R>> =
  foldToEither({ err -> err.consolidateTo(type, message, args) }, { it })

fun <E : Enum<E>, L : Fault<E>, R, T> List<Either<L, R>>.foldToEither(
  blockLeft: (List<L>) -> Fault<E>,
  blockRight: (List<R>) -> T
): Either<Fault<E>, T> =
  this.filterIsInstance<Either.Left<L>>().map { it.a }.let { lefts ->
    if (lefts.isNotEmpty()) blockLeft(lefts).left()
    else blockRight(this.filterIsInstance<Either.Right<R>>().map { it.b }).right()
  }

fun <E : Enum<E>> List<Fault<E>>.consolidateTo(type: E, message: String, args: Map<String, Any?> = Fault.EMPTY_MAP): Fault<E> =
  Fault(type, message, args, contents = this)
