package org.njk.mappers

import arrow.core.Either
import arrow.core.None
import arrow.core.Some
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer
import com.fasterxml.jackson.module.kotlin.KotlinModule
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object DataMapper {
  private val dateFormatSlashPattern = "dd/MM/yyyy"
  private val dateFormatSlash = SimpleDateFormat(dateFormatSlashPattern)

  private val module = KotlinModule()
    .addSerializer(LocalDateTime::class.java, CustomLocalDateTimeSerializer(true))
    .addSerializer(LocalDate::class.java, LocalDateSerializer(DateTimeFormatter.ofPattern(dateFormatSlashPattern)))
    .addSerializer(Either.Left::class.java, EitherSerializer())
    .addSerializer(Either.Right::class.java, EitherSerializer())

    .addSerializer(Some::class.java, OptionSerializer())
    .addSerializer(None::class.java, OptionSerializer())

    .addDeserializer(LocalDateTime::class.java, CustomLocalDateTimeDeserializer(true))
    .addDeserializer(LocalDate::class.java, LocalDateDeserializer(DateTimeFormatter.ofPattern(dateFormatSlashPattern)))
    .addDeserializer(Either::class.java, EitherDeserializer())
    .addDeserializer(Either.Right::class.java, EitherDeserializer())
    .addDeserializer(Either.Left::class.java, EitherDeserializer())

  val default: ObjectMapper = ObjectMapper()
    .registerModule(module)
    .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
    .configure(JsonParser.Feature.ALLOW_COMMENTS, true)
    .configure(JsonParser.Feature.STRICT_DUPLICATE_DETECTION, true)
    .configure(JsonParser.Feature.IGNORE_UNDEFINED, true)
    .setDateFormat(dateFormatSlash)

}
